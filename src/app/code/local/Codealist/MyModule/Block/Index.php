<?php

class Codealist_MyModule_Block_Index extends Mage_Core_Block_Template
{
    public function getProduct()
    {

        $productCollection = Mage::getModel('catalog/product')
        ->getCollection()
        ->addAttributeToSort('created_at', 'DESC')
        ->addAttributeToSelect('*')
        ->load();

        return $productCollection;
    }

}